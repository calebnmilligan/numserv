import socket
import logging
from datetime import datetime

# Set print() to output log-style with timestamp
logging.basicConfig(level=logging.INFO, format="%(message)s")
logger = logging.getLogger()
logger.addHandler(logging.FileHandler("numserv.log", "a"))
print = lambda msg: logger.info("[{}] {}".format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"), msg))


BIND_ADDR = '0.0.0.0'
BIND_PORT = 443 # Use port 80 or 443, as other ports are blocked by the firewall
PASSPHRASE = "May I please have a number?".encode("UTF-8")

# Create and bind server
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind((BIND_ADDR, BIND_PORT))
print("Bound to {}:{}".format(BIND_ADDR, BIND_PORT))
# Listen for connections
server.listen(8)
print("Listening for connections")
while True:
    # Accept client connection
    client, (address, port) = server.accept()
    print("{} connected".format(address))
    print("\tWaiting for passphrase")
    
    # Read data from client to match passphrase (don't want to accidentally issue numbers to web browsers)
    msg = client.recv(len(PASSPHRASE))

    if msg == PASSPHRASE:
        print("\tPassphrase received")
    else:
        client.close()
        print("{} disconnected: Invalid passphrase".format(address))
        continue
    # Open count.txt
    with open("count.txt", "a+") as file:
        # Parse number from file, or use 0 as default value
        file.seek(0)
        num = 0
        try:
            num = int(file.read())
        except ValueError:
            pass
        print("\tSending number: {:04d}".format(num + 1))
        # Write new number to count.txt
        file.truncate(0)
        file.write(str(num + 1))
        file.close()
        # Send number to client as UTF-8 string
        client.send(str(num + 1).encode("UTF-8"))
        # Kick the client
        client.close()
        print("{} disconnected".format(address))
