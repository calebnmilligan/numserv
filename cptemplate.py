import os
import re
import shutil
import socket

BIND_ADDR = '192.168.10.130'
BIND_PORT = 443
PASSPHRASE = "May I please have a number?".encode("UTF-8")

valid_types = [
    ["Computer", "^comp(uter)?$", "computer_specs_template.xlsx"],
    ["Computer Shell", "(^comp(uter)? ?)?sh(ell)?$", "computer_shell_template.xlsx"],
    ["Printer", "^p(rint(er)?)?$", "printer_specs_template.xlsx"],
    ["Tablet", "^t(ab(let)?)?$", "tablet_specs_template.xlsx"],
    ["Wireless Router", "^w(ireless|ifi)? ?r(outer)?$", "wireless_router_specs_template.xlsx"],
    ["Modem", "^m(odem)?$", "modem_specs_template.xlsx"],
    ["Smart Phone", "^s(mart)?p(hone)?$", "smartphone_specs_template.xlsx"],
    ["Cell Phone", "^c(ell)?p(hone)?$", "cellphone_specs_template.xlsx"],
    ["Game", "^(v(ideo)? ?)?g(ame)?$", "game_specs_template.docx"],
    ["Game Bundle", "^(v(ideo)? ?)?g(ame)? ?b(undle)?$", "game_bundle_specs_template.docx"],
    ["Small Device", "^s(mall)? ?d(evice)?$", "small_device_specs_template.docx"],
    ["Camera", "^cam(era)?$", "camera_specs_template.docx"],
    ["Digital Camera", "^d(igital)? ?cam(era)?$", "digital_camera_specs_template.xlsx"],
    ["Other", "^o(ther)?$", "generic_template.xlsx"]
]


target_directory = "."
output_directory = os.path.join(target_directory, "staged")
template_directory = os.path.join(target_directory, "templates")

template_file = None
target_file_type = None

if not os.path.exists(output_directory):
    os.mkdir(output_directory)

# Deprecated in favor of client-server model
def find_highest_number(directory: str):
    highest_number = 0
    for file in os.listdir(directory):
        num = 0
        localpath = os.path.join(directory, file)
        if os.path.isfile(localpath):
            match = re.search("^(\\d+).*", file)
            if not match:
                continue
            num = int(match.group(1))
        elif os.path.isdir(localpath):
            num = find_highest_number(localpath)
        if num > highest_number:
            highest_number = num
    return highest_number


def fetch_number():
    # Connect to remote server
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client.connect((BIND_ADDR, BIND_PORT))
    
    # Send the passphrase so the server knows we're not a web browser
    client.send(PASSPHRASE)
    
    # Parse response
    try:
        # Read data into a buffer until we reach the end of the stream
        value = bytearray()
        buffer = bytearray(2048)
        while True:
            read = client.recv_into(buffer, len(buffer))
            if read == 0:
                break
            value += buffer[:read]
        if len(value) == 0:
            print("No data received from server. Does the passphrase match?")
            exit(0)
        # Attempt to parse data
        return int(value.decode("UTF-8"))
    except ConnectionResetError:
        print("Forcibly disconnected from server. Does the passphrase match?")
        exit(0)
    except ValueError:
        print("Unknown data received from server")
        exit(0)


# Read user input
while template_file is None:
    selection = input("Select one ({}): ".format(", ".join([innerlist[0] for innerlist in valid_types])))
    # Match user input against each type's supplied regex
    for check_type in valid_types:
        if re.match(check_type[1], selection, flags=re.IGNORECASE):
            template_file = check_type[2]
            target_file_type = check_type[0].lower().replace(" ", "_")
            break

# Get the extension of the template file
_, extension = os.path.splitext(template_file)
# Get the relative path of the template file
template_file = os.path.join(template_directory, template_file)

# target_number = "{:04d}".format(find_highest_number(target_directory) + 1)
print("Fetching label number from {}:{}".format(BIND_ADDR, BIND_PORT))

# Fetch number from server
try:
    # Format number to four digits with leading zeroes
    target_number = "{:04d}".format(fetch_number())
    # Generate the name of the new file
    target_file = "{}_{}{}".format(target_number, target_file_type, extension)
    target_file = os.path.join(output_directory, target_file)
    # Copy the template file
    shutil.copy(template_file, target_file)
    
    # Open the new file with the system's default program
    os.system("start {}".format(target_file))
except ConnectionRefusedError:
    print("FATAL: Could not connect to server")
